import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import Splash from '../pages/splash/index';
import home from '../pages/home/index';
import budaya from '../pages/Budaya/index';
import lagudaerah from '../pages/Lagu Daerah/index';

const Stack =createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
            />
             <Stack.Screen
            name="home"
            component={home}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Budaya"
            component={budaya}
            />
            <Stack.Screen
            name="lagudaerah"
            component={lagudaerah}
            />
        </Stack.Navigator>
    );
};

export default Route;