import React, {useEffect} from 'react';
import {Image, StyleSheet, View} from 'react-native';


const Splash =({navigation}) => {
    useEffect(() => {
     setTimeout (() => {
         navigation.replace('home');
     },3000);
});
return (
    <View style={styles.wrapper}>
        <Image source={require('../splash/splaash.jpg')} style={{height:150,width:100}}/>
    </View>     
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor:'white',
        alignItems: 'center',
        justifyContent: 'center',
        flex :2,

    },
    logo:{
        margin: 10,
    },
});
