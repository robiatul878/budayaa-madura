import React from 'react'
import { View, Text, ScrollView } from 'react-native'

export default function index() {
    return (
        
        <View >
            <ScrollView>
            <Text style={{textAlign:'center', fontSize:16}}>TONDUK MAJENG</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>Ngapote ka' wa lajere eta ngale</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Reng majeng tan tona la pade mole</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Mon tengguh deri ombek pajelena</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Maseh benyak o angguh 'leh olehna</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Duh mon a jeling odikna o reng majengan</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>A bental ombek sapok angen salanjengan</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lir sa a lir lir sa alir alir alir</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>gung, Lir sa alir lir sa alir alir alir gung</Text>
            <Text style={{textAlign:'center', fontSize:16, marginTop:50}}>PAJJER LAGGHU</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>pajjar laggu arena pon nyonara</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>bapak tani se tedung pon jaga'a</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>ngalak arek so landuk tor capenga</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ngajalanagi sarat kawajiban</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>atatamen mabannyak hasel bumina</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>mamakmur nagarana tor bangsana</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>pajjar laggu arena pon nyonara</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>bapak tani se tedung pon jaga'a</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>ngalak arek so landuk tor capenga</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ngajalanagi sarat kawajiban</Text>
            <Text style={{textAlign:'center', fontSize:16, marginTop:50}}>POTRE MADURE</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>Raddhin koneng potre madhura </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pajalana neter kalenang</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Edheng pandheng ta’ a busenne</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Palembayanna meltas panjalin</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Alos onggu tengka gulina </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Nandha aghi tenggi darajatdha </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Adu tretan ka pencot ate </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Mon nengale potre madhura</Text>
            <Text style={{textAlign:'center', fontSize:16, marginTop:50}}>KEMBHANG MALATE POTE</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>Kembhang malate pote</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Bheuna ro’om ngapencote</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Gi buru e pettek dhari taman sare</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ropana nyenggar tor asre</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ta’ seddhe’ akadhi malate</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Menangka pangesto</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kaator ka potre</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Sa saddha’ akadhi malate</Text>
            <Text style={{textAlign:'center', fontSize:16, marginTop:50}}>LAN BULAN</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>Lan bulanan le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Padha nengkong le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>E taneyan le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ce’ sennengnga</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lan bulanan le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Padha nengko le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>E taneyan le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Senneng onggu</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ayo kanca , kanca</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Padha anga bunga</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Tera’ bulan kanca</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Tar kataran </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lan bulanan le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Padha nengkong le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>E taneyan le’</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Ramme onggu</Text>
            <Text style={{textAlign:'center', fontSize:16, marginTop:50}}>GAI BINTANG</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>Geik bintang ale’ gegger bulan</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pagei’na janor koneng</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kaka’ elang ale’ sajan jeu</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pajeuna gan lon-alon</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lea letes</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kembang kates</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lea letes</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Tocca’ toccer</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Geik bintang aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Gegger bulen aduh kak</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pageikna aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Jenor koning </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kakak elang aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Sajen jeuh aduh kak</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pajeunah aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Gen lon-alon</Text>

            <Text style={{textAlign:'center', fontSize:16, marginTop:50}}>GAI BINTANG</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic', marginTop:10}}>Geik bintang ale’ gegger bulan</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pagei’na janor koneng</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kaka’ elang ale’ sajan jeu</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pajeuna gan lon-alon</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lea letes</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kembang kates</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Lea letes</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Tocca’ toccer</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Geik bintang aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Gegger bulen aduh kak</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pageikna aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Jenor koning </Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Kakak elang aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Sajen jeuh aduh kak</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Pajeunah aduh lek</Text>
            <Text style={{textAlign:'center', fontSize:12, fontStyle:'italic'}}>Gen lon-alon</Text>
            
            </ScrollView>
        </View>
        
    )
}
