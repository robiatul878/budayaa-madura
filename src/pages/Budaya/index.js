import React from 'react'
import { View, Text, ScrollView, Image } from 'react-native'

export default function budaya() {
    return (
        <View>
            <ScrollView>
                <Text style={{textAlign:'center', fontSize:25, marginTop:5}}>KARAPAN SAPI</Text>
                <Image source={require('../Budaya/karapan.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Karapan Sapi adalah budaya suku Madura yang digelar setiap tahun pada bulan Agustus atau September. Pada perlombaan ini, sepasang sapi menarik semacam kereta dari kayu dipacu dalam lomba adu cepat melawan pasangan-pasangan sapi lain. Trek pacuan tersebut biasanya sekitar 100 meter.</Text>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Lomba pacuan dapat berlangsung sekitar sepuluh detik sampai satu menit. Beberapa kota di Madura menyelenggarakan karapan sapi pada bulan Agustus dan September setiap tahun. Final pertandingan itu pada akhir September atau Oktober di eks Kota Karesidenan, Pamekasan untuk memperebutkan piala bergilir presiden. Kini piala itu berganti nama menjadi piala gubernur.</Text>

                <Text style={{textAlign:'center', fontSize:20, marginTop:20}}>CLURIT</Text>
                <Image source={require('../Budaya/clurit.png')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Suku Madura memiliki senjata tradisional khas yang disebut clurit. Bentuk Clurit mirip dengan arit di suku Jawa yang biasa digunakan untuk bertani dan berkebun. Bedanya, clurit dari Madura lebih ramping dengan lingkar lengkung yang lebih tipis. Ujung clurit juga lebih lancip. Gagang clurit terbuat dari besi atau kayu.</Text>

                <Text style={{textAlign:'center', fontSize:20, marginTop:20}}>CAROK</Text>
                <Image source={require('../Budaya/carok.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Budaya suku Madura berikutnya yakni tradisi carok. Carok adalah duel sampai mati dengan menggunakan senjata tajam yakni celurit. Orang Madura memiliki watak keras dan mengedepankan harga diri. Karena itu, masalah diselesaikan dengan cara kekerasan.</Text>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Carok biasanya terjadi menyangkut masalah-masalah terkait kehormatan atau harga diri bagi orang Madura, seperti perselingkuhan dan harkat martabat atau kehormatan keluarga. Meski mayoritas suku Madura beragama Islam namun secara individual banyak yang masih memegang tradisi carok.</Text>

                <Text style={{textAlign:'center', fontSize:25, marginTop:20}}> UPACARA PETIK LAUT</Text>
                <Image source={require('../Budaya/laut.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Upacara petik laut yang biasa disebut Rokat Tase. Tradisi ini merupakan ungkapan rasa syukur atas karunia dan nikmat yang diberikan oleh Tuhan Tradisi ini juga dipercaya dapat memberikan keselamatan dan kelancaran rezeki.</Text>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Tradisi Upacara Petik Laut dimulai dengan acara pembacaan istighosah dan tahlil bersama masyarakat dengan dipimpin pemuka agama. Setelah itu, masyarakat menghanyutkan sesaji ke laut sebagai ungkapan rasa syukur kepada Tuhan. Isi dari sesaji itu adalah tumpeng, ketan berwarna-warni, dan ikan-ikan.</Text>

                <Text style={{textAlign:'center', fontSize:25, marginTop:20}}>SAPI SONOK</Text>
                <Image source={require('../Budaya/sapi.jpeg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Selain kerapan sapi orang Madura juga memiliki budaya lain yang masih berhubungan dengan sapi, yaitu sapi sonok. Sapi sonok ini merupakan kontes kecantikan sapi betina, disini para sapi betina didandani layaknya pengantin dengan dibantu oleh pawang dan musik saronen sehingga sapi betina tersebut berlenggak-lenggok layaknya model, disamping dandanan dan penampilan sapi, kesehatan sapi itu sendiri juga merupakan salah satu poin penting yang dilombakan.</Text>

                <Text style={{textAlign:'center', fontSize:25, marginTop:20}}>MUSIK SARONIN</Text>
                <Image source={require('../Budaya/saronin.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Musik saronen adalah seni musik dan tarian yang menjadi pengiring dalam lomba kerapan sapi maupun sapi sonok sebelum maju ke medan laga, dalam kesenian musik saronen ini didominasi oleh suara terompet (saronen dalam bahasa Madura) dan tabuhan gong yang bertalu-talu. Biasanya musik saronen dimainkan oleh kaum pria dengan dandanan yang mencolok dan pakaian dengan warna yang menyilaukan mata (ngejreng) sehingga nampak meriah.</Text>

                <Text style={{textAlign:'center', fontSize:25, marginTop:20}}>TARI MUANG SANGKAL</Text>
                <Image source={require('../Budaya/tari.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Tari muang sangkal (tari buang sial), tarian ini merupakan tarian khas dari kabupaten Sumenep yang sering ditampilkan untuk menyambut tamu agung, tarian ini menggambarkan harapan dan doa masyarakat Sumenep kepada Tuhan Yang Maha Esa agar terhindar dari bencana, dalam perkembangannya tari muang sangkal juga biasa dimainkan saat acara pernikahan dan lain sebagainya.</Text>

                <Text style={{textAlign:'center', fontSize:25, marginTop:20}}>TOPENG DHALANG</Text>
                <Image source={require('../Budaya/topeng.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Topeng Dhalang merupakan kesenian tradisional berupa teater rakyat, biasanya cerita yang diangkat merupakan cerita yang berasal dari kisah Mahabarata, dalam kesenian topeng dalang ini musik pengiringnya menggunakan gamelan dan seluruh pemainnya biasanya kaum lelaki, semua penari menggunakan topeng kayu yang dihasilkan oleh pengrajin setempat.</Text>

                <Text style={{textAlign:'center', fontSize:25, marginTop:20}}>TEMBHENG MADURE</Text>
                <Image source={require('../Budaya/macopat.jpg')} style={{height:225,width:350, marginTop:10}}></Image>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Tembheng Macapat Madura memiliki ikatan aturan, Tembheng yaitu jumlah gatra (padde) dari masing – masing tembheng berbeda, mengikuti aturan guru lagu dan guru wilangan yang sama. Tembheng Macapat Madura memiliki keunikan tersendiri yaitu lebih diutamakan cengkok atau lagu pada tembheng itu.</Text>
                <Text style={{ fontSize:15, fontStyle:'italic', marginTop:10}}>Tembheng biasanya digelar warga Madura ketika memiliki hajatan. Nur (52), satu tokoh pentas Tembheng Macapat yang berasal dari Kecamatan Gapura, Sumenep, mengatakan, “Kesenian Tembheng Macapat Madura ini masih dijaga oleh masyarakat Madura. Kesenian Tembheng Macapat Madura ini sudah saya naungi selama 10 tahun. Saya bisa melestarikan kesenian ini bersama teman-teman pemain lainnya,” tutur Nur, satu anggota Kesenian Tembheng Madura.</Text>
            </ScrollView>
        </View>
    )
}
