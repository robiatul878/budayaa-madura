import React from 'react'
import { View, Text, } from 'react-native'
import {TouchableOpacity } from 'react-native-gesture-handler'


const home = ({navigation}) => {
    return (
        <View style={{marginTop:250,}}>
            <TouchableOpacity style={{backgroundColor:'brown',marginHorizontal:70,height:'40%',justifyContent:'center',borderRadius:30,}} onPress={() =>navigation.navigate('Budaya')}>
                <Text style={{textAlign:'center',textTransform:'uppercase',fontWeight:'bold',fontSize:15}}>Budaya</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{backgroundColor:'brown',marginHorizontal:70,height:'40%',justifyContent:'center',borderRadius:30,}} onPress={() =>navigation.navigate('lagudaerah')}>
                <Text style={{textAlign:'center',textTransform:'uppercase',fontWeight:'bold',fontSize:15}}>Lagu Daerah</Text>
            </TouchableOpacity>

        </View>
    );
};
export default home;
